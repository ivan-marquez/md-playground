module.exports = function (grunt) {

	require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		express: {
			all: {
				options: {
					bases: ['/Users/Ivan/Dropbox/Development/Repos/Markdown'], // /Users/Ivan/Dropbox/Development/Repos/Markdown  C:\\Users\\jm5256\\Source\\Repos\\MarkDown
					port: 8080,
					hostname: "0.0.0.0",
					livereload: true
				}
			}
		},
		watch: {
			all: {
				files: '**/*.html',
				options: {
					livereload: true
				}
			},
			md2html: {
				files: 'md/*.md',
				tasks: ['md2html:docs']
			},
			livereload: {
				files: ['*.html', 'md/*.md', '*.php', 'js/**/*.{js,json}', 'css/*.css', 'img/**/*.{png,jpg,jpeg,gif,webp,svg}'],
				options: {
					livereload: true,
					markedOptions: {
						gfm: true,
						langPrefix: 'code-'
					}
				}
			}
		},
		md2html: {
			docs: {
				options: {
					layout: "html/layout.html"
				},
				files: [{
					expand: true,
					cwd: 'md',
                    src: ['**/*.md'],
                    dest: 'html',
					ext: '.html'
                }]
			}
		},
		open: {
			all: {
				path: 'http://localhost:8080/index.html'
			}
		}
	});
	grunt.registerTask('init', ['express', 'open', 'md2html:docs', 'watch']);
	grunt.loadNpmTasks('grunt-contrib-watch');
};