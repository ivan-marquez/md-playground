# Using TDD and the Red-Green-Refactor Workflow

---

With test-driven development (TDD), you use unit tests to help design your code. This can be an odd concept if you are used to testing after you have finished coding, but there is a lot of sense in this approach. The key concept is a development workflow called **red-green-refactor**. It works like this:  

+ Determine that you need to add a new feature or method to your application.
+ Write the test that will validate the behavior of the new feature when it is written.
+ Run the test and get a red light.
+ Write the code that implements the new feature.
+ Run the test again and correct the code until you get a green light.
+ Refactor the code if required. For example, reorganize the statements, rename the variables, and so on.
+ Run the test to confirm that your changes have not changed the behavior of your additions.  

This workflow is repeated for every feature you add. TDD inverts the traditional development process: you start by  writing tests for the perfect implementation of a feature, knowing that the tests will fail. You then implement the  feature, creating each aspect of its behavior to pass one or more tests.  

This cycle is the essence of TDD. There is a lot to recommend it as a development style, not least because it makes  a programmer think about how a change or enhancement should behave before the coding starts. You always have a clear  end-point in view and a way to check that you are there. And if you have unit tests that cover the rest of your application, you can be sure that your additions have not changed the behavior elsewhere.  

**The drawback of TDD is that it requires discipline**. As deadlines get closer, the temptation is always to discard TDD and just start writing code or, as I have witnessed several times on projects, sneakily discard problematic tests to make code appear in better shape than it really is. For these reasons, TDD should be used in established and mature development teams where there is generally a high level of skill and discipline or in teams where there the team leads can enforce good practice, even in the face of time constraints.  

