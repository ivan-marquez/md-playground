# Markdown

This a **simple** example of Markdown.

## Features:
- Simple
- Fast
- Portable
Check the [official website].

[official website]: http://daringfireball.net/projects/markdown/


Two or more spaces at the end of the line create a line break:

A paragraph,
divided into two lines.

A paragraph

Another paragraph, separated by a new line.

A sentence,  
with a line break.

Setext Style

Header 1
========

Header 2
--------

Atx style

# Header 1
## Header 2
### Header 3
#### Header 4
##### Header 5
###### Header 6

Optionally, we may close atx hash headers:

# Header 1 #
## Header 2 ##
### Header 3 ###
#### Header 4 ####
##### Header 5 #####
###### Header 6 ######

The number of closing hashes doesn't matter.


*single asterisks*  
_single underscores_  
**double asterisks**  
__double underscores__  


Ordered lists use numbers as list markers:

1. One
2. Two
6. Six

The actual numbers you use to mark the list have no effect.  

Unordered lists use asterisks (*), plus signs (+), and hyphens (-) as list markers:

* A list.
* Second element.
+ A new entry.
- Another entry in a list.
- And the list goes on.  

Asterisks, plus signs, and hyphens are interchangeable.  

List can be nested as follows:

- First level
    - Second level
        - Third level
    - Second level again  
	  
	
**Inline links**

An example of an [inline link](http://example.com "Example").
[This link](http://example.net/) has no title attribute.

---

## Reference links  

This is [an example] [id] reference-style link.

[id]: https://google.com "Google (optional title)"

---

## Automatic Links

<https://google.com>

---

## Blockquotes

> A blockquote
>> Nested blockquote

---

## Code spans

Type `echo 'hello world!` in your terminal.

---

## Image links  

### Inline  

![Html 5](../img/html.png "Optional title")

### Reference  

![Html 5][html]

[html]: ../img/html.png "Optional title"

---

## Horizontal Rules  

---

## Inserting Html  

This is a paragraph.
<table>
  <tr>
    <td>Row</td>
  </tr>
</table>

This is another paragraph.

--- 

## Github Flavored Markdown (GFM) 

### Codeblocks with syntax highlighting  


```javascript
grunt.initConfig({
  sass: {
    dist: {
      files: [{
        expand: true,
        cwd: 'styles',
        src: ['*.scss'],
        dest: '../public',
        ext: '.css'
      }]
    }
  }
});
```

---

## Tables  

## Options / API

There are currently two ways to use this component. First up is by using html5 `data-*` attributes embedded in the
select tag, but you can also provide a Javascript Object. The variable names are the same all around and can be even
mixed and matched within the select or Javascript. The current options are: 

| Option       | Type    | Default    | Description |
| ------------ | ------- | ---------- | ----------- |
| `json`       | Boolean | true       | Whether to download the data via a JSON request.  |
| `uri`        | URI     | local.json | The relative or absolute URI where to receive the data from. | 
| `value`      | String  | id         | This determines what JSON field is handled as the value. |
| `text`       | String  | name       | This determines what JSON field is handled as the text. | 
| `title`      | String  | Example    | The title of the control. |
| `horizontal` | Boolean | false      | Whether the control is lay out horizontal or vertical. |
| `timeout`    | UInt    | 500        | Timeout when to start searching with the filter. |
| `textLength` | UInt    | 45         | Maximum text length of when the element should contain title-attributes. |
| `moveAllBtn` | Boolean | true       | Whether to display the move all button (from left to right or vice-versa). |
| `maxAllBtn`  | UInt    | 500        | Integer to determine from which length to display the warning message below. |
| `warning`    | String  | <...>      | Warning message that is displayed when trying to move large amounts of elements. |