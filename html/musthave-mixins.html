<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="../css/bootstrap.css">
        <link rel="stylesheet" href="../css/tomorrow-night.css">
        <title>Markdown</title>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 id="8-sass-mixins-you-must-have-in-your-toolbox">8 Sass mixins you must have in your toolbox</h1>
<h3 id="1-set-a-rem-font-size-with-pixel-fallback">1. Set a rem font size with pixel fallback</h3>
<p>Rem is similar to the em value, but instead of being relative to the parent element,<br>it’s relative to the font-size set in the <code>&lt;html&gt;</code>.</p>
<p>It has all the benefits of <strong>em</strong> but you don’t get issues with e.g <a href="http://snook.ca/archives/html_and_css/font-size-with-rem">compounding</a> since rem is<br>only relative to the html element. The bad part is there’s no support for rem units in IE8 and below.<br>But with this mixin <strong>we can create a fallback to pixels</strong> when rem isn’t supported.</p>
<pre><code class="lang-scss">@function calculateRem($size) {
  $remSize: $size / 16px;
  @return $remSize * 1rem;
}

@mixin font-size($size) {
  font-size: $size;
  font-size: calculateRem($size);
}
</code></pre>
<h4 id="usage">Usage</h4>
<pre><code class="lang-scss">p {
  @include font-size(14px)
}
</code></pre>
<h4 id="output">Output</h4>
<pre><code class="lang-css">p {
  font-size: 14px; //Will be overridden if browser supports rem
  font-size: 0.8rem;
}
</code></pre>
<h3 id="2-breakpoints">2. Breakpoints</h3>
<p>When Sass 3.2 was released some time ago, they made it possible to define names to our media queries, which<br>makes the usage of them a lot cleaner. Instead of calling them <em>@media (min-width: 600px)</em> we can give them<br>more semantic names like <em>“breakpoint-large”</em> or <em>“breakpoint-a-really-large-computer-machine”.</em></p>
<pre><code class="lang-scss">@mixin bp-large {
  @media only screen and (max-width: 60em) {
    @content;
  }
}

@mixin bp-medium {
  @media only screen and (max-width: 40em) {
    @content;
  }
}

@mixin bp-small {
  @media only screen and (max-width: 30em) {
    @content;
  }
}
</code></pre>
<h4 id="usage">Usage</h4>
<pre><code class="lang-scss">.sidebar {
  width: 60%;
  float: left;
  margin: 0 2% 0 0;
  @include bp-small {
    width: 100%;
    float: none;
    margin: 0;
  }
}
</code></pre>
<h4 id="output">Output</h4>
<pre><code class="lang-css">.sidebar {
  width: 60%;
  float: left;
  margin: 0 2% 0 0;
  @media only screen and (max-width: 30){
    .sidebar{width: 100%; float: none; margin: 0;}
  }
}
</code></pre>
<h3 id="3-svg-background-images-with-png-and-retina-fallback">3. SVG background images with PNG and retina fallback</h3>
<p>This mixin depends on <strong>Modernizr</strong> and creates a bit more work for you when creating images for your site,<br>but it’s really worth it in the end.  </p>
<p>You need <strong>one .svg file, that will serve as the default background image.</strong> You’ll also need <strong>a regular .png that<br>serves as a fallback for non-svg-supporting browsers.</strong> And last you need <strong>a twice as large .png as a second fallback to retina screens.</strong></p>
<p>All in all you need this:  </p>
<ul>
<li>pattern.svg</li>
<li>pattern.png</li>
<li>pattern@2x.png  </li>
</ul>
<pre><code class="lang-scss">$image-path: &#39;../img&#39; !default;
$fallback-extension: &#39;png&#39; !default;
$retina-suffix: &#39;@2x&#39;;

@mixin background-image($name, $size:false){
    background-image: url(#{$image-path}/#{$name}.svg);
    @if($size){
        background-size: $size;
    }
    .no-svg &amp;{
        background-image: url(#{$image-path}/#{$name}.#{$fallback-extension});

        @media only screen and (-moz-min-device-pixel-ratio: 1.5), only screen and (-o-min-device-pixel-ratio: 3/2), only screen and (-webkit-min-device-pixel-ratio: 1.5), only screen and (min-device-pixel-ratio: 1.5) {
          background-image: url(#{$image-path}/#{$name}#{$retina-suffix}.#{$fallback-extension});
        }
    }
}
</code></pre>
<h4 id="usage">Usage</h4>
<pre><code class="lang-scss">body {
  @include background-image(&#39;pattern&#39;);
}
</code></pre>
<h3 id="4-animations-and-keyframes">4. Animations and keyframes</h3>
<p>Animations are always a pain to create <strong>with all the vendor prefixes and what not.</strong> But with<br>the help of this mixin it will boil down to just a few lines of code.  </p>
<pre><code class="lang-scss">@mixin keyframes($animation-name) {
    @-webkit-keyframes #{$animation-name} {
        @content;
    }
    @-moz-keyframes #{$animation-name} {
        @content;
    }  
    @-ms-keyframes #{$animation-name} {
        @content;
    }
    @-o-keyframes #{$animation-name} {
        @content;
    }  
    @keyframes #{$animation-name} {
        @content;
    }
}

@mixin animation($str) {
  -webkit-animation: #{$str};
  -moz-animation: #{$str};
  -ms-animation: #{$str};
  -o-animation: #{$str};
  animation: #{$str};      
}
</code></pre>
<h4 id="usage">Usage</h4>
<pre><code class="lang-scss">@include keyframes(slide-down) {
  0% { opacity: 1; }
  90% { opacity: 0; }
}

.element {
  width: 100px;
  height: 100px;
  background: black;
  @include animation(&#39;slide-down 5s 3&#39;);
}
</code></pre>
<h3 id="5-transitions">5. Transitions</h3>
<p>As with animations, transitions also make your code quite bloated which can hurt the readability.<br>But this is also solved by using a mixin for it.  </p>
<pre><code class="lang-scss">@mixin transition($args...) {
  -webkit-transition: $args;
  -moz-transition: $args;
  -ms-transition: $args;
  -o-transition: $args;
  transition: $args;
}
</code></pre>
<h4 id="usage">Usage</h4>
<pre><code class="lang-scss">a {
  color: gray;
  @include transition(color .3s ease);
  &amp;:hover {
    color: black;
  }
}
</code></pre>
<h3 id="6-cross-browser-opacity">6. Cross browser opacity</h3>
<p><strong>This mixin ensures cross browser opacity all the way down to Internet Explorer 5</strong>. Though<br>if you have to optomize for IE5, you have a lot bigger problems than opacity, godspeed my friend.</p>
<pre><code class="lang-scss">@mixin opacity($opacity) {
  opacity: $opacity;
  $opacity-ie: $opacity * 100;
  filter: alpha(opacity=$opacity-ie); //IE8
}
</code></pre>
<h4 id="usage">Usage</h4>
<pre><code class="lang-scss">.faded-text {
  @include opacity(0.8);
}
</code></pre>
<h3 id="7-clearfix">7. Clearfix</h3>
<p>There’s a lot of different clearfix hacks out there on the web. This one is created by <a href="http://nicolasgallagher.com/micro-clearfix-hack/">Nicolas Gallagher</a><br>and I’ve found it to be the most solid one. Works in IE6 and up.</p>
<h4 id="usage">Usage</h4>
<pre><code class="lang-scss">.container-with-floated-children {
  @extend %clearfix;
}
</code></pre>
<h3 id="8-visually-hide-an-element">8. Visually hide an element</h3>
<p>When you hide an element with display: none, that prevents screen readers from reading it to the user.<br>Sometimes that’s fine, but in other cases this will make the site hard to use for people with screen readers.  </p>
<p>Thus, we have to use another technique for hiding elements while at the same time make them <a href="https://css-tricks.com/places-its-tempting-to-use-display-none-but-dont/">accessible</a>.<br>In this example, we are using the <a href="http://sass-lang.com/documentation/file.SASS_REFERENCE.html#placeholder_selectors_">Sass placeholder selector</a> since the output will always be<br>the same, which enables us to reduce repetetive code in the output.</p>
<pre><code class="lang-scss">%visuallyhidden {
  margin: -1px;
  padding: 0;
  width: 1px;
  height: 1px;
  overflow: hidden;
  clip: rect(0 0 0 0);
  clip: rect(0, 0, 0, 0);
  position: absolute;
}
</code></pre>
<h4 id="usage">Usage</h4>
<pre><code class="lang-html">&lt;button class=&quot;mobile-navigation-trigger&quot;&gt;
  &lt;b class=&quot;visually-hidden&quot;&gt;Open the navigation&lt;/b&gt;
  &lt;img src=&quot;img/mobile-navigation-icon.svg&quot;&gt;
&lt;/button&gt;
</code></pre>
<pre><code class="lang-scss">.visually-hidden {
  @extend %visuallyhidden;
}
</code></pre>

                </div>
            </div>
        </div>
        <script src="../js/jquery.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/highlight.pack.js"></script>
        <script>hljs.initHighlightingOnLoad();</script>
    </body>
</html>